package com.szh.eduorder.mapper;

import com.szh.eduorder.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-14
 */
public interface OrderMapper extends BaseMapper<Order> {

}
