package com.szh.eduorder.client;

import com.szh.commonutils.CourseWebVoOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 */
@Component
@FeignClient(name = "service-edu")
public interface EduClient {

    //根据课程id查询课程信息
    @PostMapping(value = "/eduservice/coursefront/getCourseInfoOrder/{id}")
    public CourseWebVoOrder getCourseInfoOrder(@PathVariable("id") String id);

}
