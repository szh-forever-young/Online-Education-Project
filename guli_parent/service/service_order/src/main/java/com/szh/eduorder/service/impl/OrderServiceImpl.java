package com.szh.eduorder.service.impl;

import com.szh.commonutils.CourseWebVoOrder;
import com.szh.commonutils.UcenterMemberOrder;
import com.szh.eduorder.client.EduClient;
import com.szh.eduorder.client.UcenterClient;
import com.szh.eduorder.entity.Order;
import com.szh.eduorder.mapper.OrderMapper;
import com.szh.eduorder.service.OrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.eduorder.utils.OrderNoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-14
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private EduClient eduClient;

    @Autowired
    private UcenterClient ucenterClient;

    //生成订单
    @Override
    public String createOrder(String courseId, String memberId) {
        //通过远程调用，根据用户id获取用户信息
        UcenterMemberOrder userInfoOrder = ucenterClient.getUserInfoOrder(memberId);

        //通过远程调用，根据课程id获取课程信息
        CourseWebVoOrder courseInfoOrder = eduClient.getCourseInfoOrder(courseId);

        //创建Order对象，向Order对象中添加数据
        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());//订单号
        order.setCourseId(courseId); //课程id
        order.setCourseTitle(courseInfoOrder.getTitle()); //课程标题
        order.setCourseCover(courseInfoOrder.getCover()); //课程封面
        order.setTotalFee(courseInfoOrder.getPrice()); //课程价格
        order.setTeacherName(courseInfoOrder.getTeacherName()); //讲师名称
        order.setMemberId(memberId); //用户id
        order.setMobile(userInfoOrder.getMobile()); //用户手机号
        order.setNickname(userInfoOrder.getNickname()); //用户昵称
        order.setStatus(0);  //订单状态（0：未支付 1：已支付）
        order.setPayType(1);  //支付类型 ，微信1
        baseMapper.insert(order);

        //返回订单号
        return order.getOrderNo();
    }
}
