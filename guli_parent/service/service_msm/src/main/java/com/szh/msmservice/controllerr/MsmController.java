package com.szh.msmservice.controllerr;

import com.szh.commonutils.R;
import com.szh.msmservice.service.MsmService;
import com.szh.msmservice.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

/**
 *
 *
 */
@RestController
@RequestMapping(value = "/edumsm/msm")
//@CrossOrigin
public class MsmController {

    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    //发送验证码
    @GetMapping(value = "/send/{mobile}")
    public R sendMsm(@PathVariable String mobile) {
        //从redis获取验证码，如果获取到直接返回
        String code = redisTemplate.opsForValue().get(mobile);
        if (!StringUtils.isEmpty(code)) {
            return R.ok();
        } else { //如果redis获取不到, 手动添加验证码随机值到redis中
            code = RandomUtil.getFourBitRandom();
            //设置有效时间为5分钟
            redisTemplate.opsForValue().set(mobile,code,5, TimeUnit.MINUTES);
            return R.ok();
        }
    }

}
