package com.szh.eduservice.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.szh.commonutils.R;
import com.szh.eduservice.service.VodService;
import com.szh.eduservice.utils.ConstantVodUtils;
import com.szh.eduservice.utils.InitVodClient;
import com.szh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 *
 */
@RestController
@RequestMapping(value = "/eduvod/video")
//@CrossOrigin
public class VodController {

    @Autowired
    private VodService vodService;

    //上传视频到阿里云
    @PostMapping(value = "/uploadAlyVideo")
    public R uploadAlyVideo(MultipartFile file) {
        //返回上传视频的id值
        String videoId = vodService.uploadAlyVideo(file);
        return R.ok().data("videoId",videoId);
    }

    //根据视频id删除阿里云视频
    @DeleteMapping(value = "/removeAlyVideo/{id}")
    public R removeAlyVideo(@PathVariable String id) {
        vodService.removeAlyVideo(id);
        return R.ok();
    }

    //删除多个阿里云视频
    @DeleteMapping(value = "/delete-batch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList) {
        vodService.removeMoreAlyVideo(videoIdList);
        return R.ok();
    }

    //根据视频id获取视频凭证
    @GetMapping(value = "/getPlayAuth/{id}")
    public R getPlayAuth(@PathVariable String id) {
        try {
            //创建初始化对象
            DefaultAcsClient client = InitVodClient.initVodClient(ConstantVodUtils.ACCESS_KEY_ID, ConstantVodUtils.ACCESS_KEY_SECRET);
            //创建获取凭证的request对象
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            //将视频id存入request对象中
            request.setVideoId(id);
            //调用方法得到视频凭证, 创建获取凭证的response对象
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);
            String playAuth = response.getPlayAuth();
            return R.ok().data("playAuth",playAuth);
        } catch (Exception e) {
            throw new GuliException(20001,"获取视频凭证失败....");
        }
    }

}
