package com.szh.demo.excel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class TestEasyExcel {

    public static void main(String[] args) {
        //实现Excel的写操作
        //设置写入文件夹的地址、文件名称
        //String fileName = "F:/write.xlsx";

        //实现写操作
        //EasyExcel.write(fileName,DemoData.class).sheet("学生列表").doWrite(getData());

        //实现Excel的读操作
        String fileName = "F:/write.xlsx";

        //实现读操作
        EasyExcel.read(fileName,DemoData.class,new ExcelListener()).sheet().doRead();
    }

    //创建方法，返回Excel所需的List集合数据
    private static List<DemoData> getData() {
        List<DemoData> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DemoData data = new DemoData();
            data.setSno(i);
            data.setSname("lucy" + i);
            list.add(data);
        }
        return list;
    }
}
