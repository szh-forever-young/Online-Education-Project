package com.szh.eduservice.service;

import com.szh.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.eduservice.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
public interface EduChapterService extends IService<EduChapter> {

    //课程大纲列表，根据课程id先将对应课程查询出来
    List<ChapterVo> getChapterVideoByCourseId(String courseId);

    //删除章节
    boolean deleteChapter(String chapterId);

    //根据课程id删除课程中的章节
    void removeChapterByCourseId(String courseId);
}
