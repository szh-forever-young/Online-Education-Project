package com.szh.eduservice.mapper;

import com.szh.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.szh.eduservice.entity.frontvo.CourseWebVo;
import com.szh.eduservice.entity.vo.CoursePublishVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    //根据课程id查询课程的相关信息
    CoursePublishVo getPublishCourseInfo(@Param("id") String courseId);

    //根据课程id，编写sql语句查询课程信息
    CourseWebVo getBaseCourseInfo(@Param("courseId") String courseId);
}
