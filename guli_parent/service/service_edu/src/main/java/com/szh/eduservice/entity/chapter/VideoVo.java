package com.szh.eduservice.entity.chapter;

import lombok.Data;

/**
 * 二级标题：小节类
 */
@Data
public class VideoVo {

    private String id;

    private String title;

    private String videoSourceId;
}
