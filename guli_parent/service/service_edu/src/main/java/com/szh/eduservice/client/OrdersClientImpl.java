package com.szh.eduservice.client;

import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public class OrdersClientImpl implements OrdersClient{
    @Override
    public boolean isBuyCourse(String courseId, String memberId) {
        System.out.println("支付失败, 可能是网络原因或其他结果导致, 请稍后再试....");
        return false;
    }
}
