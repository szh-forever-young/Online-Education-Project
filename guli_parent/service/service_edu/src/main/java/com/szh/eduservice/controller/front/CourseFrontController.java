package com.szh.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.szh.commonutils.CourseWebVoOrder;
import com.szh.commonutils.JwtUtils;
import com.szh.commonutils.R;
import com.szh.eduservice.client.OrdersClient;
import com.szh.eduservice.entity.EduCourse;
import com.szh.eduservice.entity.chapter.ChapterVo;
import com.szh.eduservice.entity.frontvo.CourseFrontVo;
import com.szh.eduservice.entity.frontvo.CourseWebVo;
import com.szh.eduservice.service.EduChapterService;
import com.szh.eduservice.service.EduCourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 *
 */
@RestController
@RequestMapping("/eduservice/coursefront")
//@CrossOrigin
public class CourseFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrdersClient ordersClient;

    //条件查询带分页查询课程
    @PostMapping(value = "/getFrontCourseList/{page}/{limit}")
    public R getFrontCourseList(@PathVariable long page,
                                @PathVariable long limit,
                                @RequestBody(required = false) CourseFrontVo courseFrontVo) {
        Page<EduCourse> pageCourse = new Page<>(page,limit);
        Map<String,Object> map = courseService.getFrontCourseList(pageCourse,courseFrontVo);
        return R.ok().data(map);
    }

    //查询课程详情信息
    @GetMapping(value = "/getFrontCourseInfo/{courseId}")
    @HystrixCommand(fallbackMethod = "getFrontCourseInfoFallbackMethod",
                    commandProperties = {
                        @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="3500")
                    })
    public R getFrontCourseInfo(@PathVariable String courseId, HttpServletRequest request) {
        //根据课程id，编写sql语句查询课程信息
        CourseWebVo courseWebVo = courseService.getBaseCourseInfo(courseId);
        //根据课程id查询章节和小节
        List<ChapterVo> chapterVideoList = chapterService.getChapterVideoByCourseId(courseId);
        //根据课程id、用户id查询当前课程是否已经支付
        boolean buyCourse = ordersClient.isBuyCourse(courseId, JwtUtils.getMemberIdByJwtToken(request));

        return R.ok().data("courseWebVo",courseWebVo)
                .data("chapterVideoList",chapterVideoList)
                .data("isBuy",buyCourse);
    }
    public R getFrontCourseInfoFallbackMethod(@PathVariable String courseId, HttpServletRequest request) {
        System.out.println("查询课程详情信息已出错, 即将触发服务降级....");
        return R.error();
    }

    //根据课程id查询课程信息
    @PostMapping(value = "/getCourseInfoOrder/{id}")
    public CourseWebVoOrder getCourseInfoOrder(@PathVariable String id) {
        CourseWebVo courseWebVo = courseService.getBaseCourseInfo(id);
        CourseWebVoOrder courseWebVoOrder = new CourseWebVoOrder();
        BeanUtils.copyProperties(courseWebVo,courseWebVoOrder);
        return courseWebVoOrder;
    }

}
