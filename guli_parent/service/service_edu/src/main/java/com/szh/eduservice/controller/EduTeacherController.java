package com.szh.eduservice.controller;

import com.szh.commonutils.R;
import com.szh.eduservice.entity.EduTeacher;
import com.szh.eduservice.entity.vo.TeacherQuery;
import com.szh.eduservice.service.EduTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-08-30
 */
@Api(description = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
//@CrossOrigin
public class EduTeacherController {

    @Autowired
    private EduTeacherService teacherService;

    //1.查询全部讲师
    @ApiOperation(value = "获取所有讲师列表")
    @GetMapping(value = "/findAll")
    public R findAllTeacher() {
        List<EduTeacher> list = teacherService.list(null);
        return R.ok().data("items",list);
    }

    //2.逻辑删除某个讲师
    @ApiOperation(value = "根据id逻辑删除讲师")
    @DeleteMapping(value = "/{id}")
    public R removeTeacher(@ApiParam(name = "id",value = "讲师ID",required = true)
                           @PathVariable("id") String id) {
        boolean flag = teacherService.removeById(id);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    //3.分页查询讲师
    //current 当前页, limit 每页的记录数
    @ApiOperation(value = "分页讲师列表")
    @GetMapping(value = "/pageTeacher/{current}/{limit}")
    public R pageListTeacher(@ApiParam(name = "current",value = "当前页",required = true)
                             @PathVariable("current") long current,
                             @ApiParam(name = "limit",value = "每页的记录数",required = true)
                             @PathVariable("limit") long limit) {
        Map map = teacherService.pageTeacher(current,limit);
        return R.ok().data(map);
    }

    //4.条件查询（附带分页）
    @ApiOperation(value = "条件查询讲师，列表分页显示")
    @PostMapping(value = "/pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition(@ApiParam(name = "current",value = "当前页",required = true)
                                  @PathVariable("current") long current,
                                  @ApiParam(name = "limit",value = "每页的记录数",required = true)
                                  @PathVariable("limit") long limit,
                                  @RequestBody(required = false) TeacherQuery teacherQuery) {
        Map map = teacherService.pageTeacherCondition(current,limit,teacherQuery);
        return R.ok().data(map);
    }

    //5.添加讲师
    @ApiOperation(value = "添加讲师")
    @PostMapping(value = "/addTeacher")
    public R addTeacher(@ApiParam(name = "teacher",value = "讲师对象",required = true)
                        @RequestBody EduTeacher eduTeacher) {
        boolean save = teacherService.save(eduTeacher);
        if (save) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    //6.根据id查询讲师
    @ApiOperation(value = "根据id查询讲师")
    @GetMapping(value = "/getTeacher/{id}")
    public R getById(@ApiParam(name = "id",value = "讲师ID",required = true)
                     @PathVariable("id") String id) {
        EduTeacher eduTeacher = teacherService.getById(id);
        return R.ok().data("teacher",eduTeacher);
    }

    //7.根据id修改讲师
    @ApiOperation(value = "根据id修改讲师")
    @PostMapping(value = "/updateTeacher")
    public R updateById(@RequestBody EduTeacher eduTeacher) {
        boolean flag = teacherService.updateById(eduTeacher);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

}
