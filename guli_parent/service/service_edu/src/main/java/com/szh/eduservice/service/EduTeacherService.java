package com.szh.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.szh.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.eduservice.entity.vo.TeacherQuery;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-08-30
 */
public interface EduTeacherService extends IService<EduTeacher> {

    //分页查询讲师
    Map pageTeacher(long current, long limit);

    //条件查询（附带分页）
    Map pageTeacherCondition(long current, long limit, TeacherQuery teacherQuery);

    //查询前4位名师
    List<EduTeacher> getTeacherList();

    //分页查询讲师
    Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageTeacher);
}
