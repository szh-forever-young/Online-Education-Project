package com.szh.eduservice.controller;


import com.szh.commonutils.R;
import com.szh.eduservice.entity.subject.OneSubject;
import com.szh.eduservice.service.EduSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-03
 */
@RestController
@RequestMapping("/eduservice/subject")
//@CrossOrigin
public class EduSubjectController {

    @Autowired
    private EduSubjectService subjectService;

    //添加课程分类
    @PostMapping(value = "/addSubject")
    public R addSubject(MultipartFile file) {
        //获取上传过来的excel文件
        subjectService.saveSubject(file,subjectService);
        return R.ok();
    }

    //课程分类列表
    @GetMapping(value = "/getAllSubject")
    public R getAllSubject() {
        //一级分类中包含二级分类，这里的泛型声明为一级分类即可
        List<OneSubject> list = subjectService.getAllOneTwoSubject();
        return R.ok().data("list",list);
    }

}

