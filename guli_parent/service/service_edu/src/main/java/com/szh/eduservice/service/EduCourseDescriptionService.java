package com.szh.eduservice.service;

import com.szh.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
