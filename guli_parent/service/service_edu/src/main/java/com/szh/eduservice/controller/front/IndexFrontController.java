package com.szh.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.szh.commonutils.R;
import com.szh.eduservice.entity.EduCourse;
import com.szh.eduservice.entity.EduTeacher;
import com.szh.eduservice.service.EduCourseService;
import com.szh.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 操作前台的controller
 */
@RestController
@RequestMapping(value = "/eduservice/indexfront")
//@CrossOrigin
public class IndexFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduTeacherService teacherService;

    //查询前8条热门课程、前4位名师
    @GetMapping(value = "/index")
    public R index() {
        //查询前8条热门课程
        List<EduCourse> eduList = courseService.getCourseList();

        //查询前4位名师
        List<EduTeacher> teacherList = teacherService.getTeacherList();

        return R.ok().data("eduList",eduList).data("teacherList",teacherList);
    }
}
