package com.szh.eduservice.mapper;

import com.szh.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
