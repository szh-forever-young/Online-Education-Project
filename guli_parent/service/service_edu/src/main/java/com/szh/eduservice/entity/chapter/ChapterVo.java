package com.szh.eduservice.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 一级标题：章节类
 */
@Data
public class ChapterVo {

    private String id;

    private String title;

    //每个章节中存在多个小节
    private List<VideoVo> children = new ArrayList<>();
}
