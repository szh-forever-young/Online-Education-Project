package com.szh.eduservice.client;

import com.szh.commonutils.R;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 */
@Component
public class VodFileDegradeFeignClient implements VodClient {

    @Override
    public R removeAlyVideo(String id) {
        System.out.println("已触发服务降级....");
        return R.error().message("删除视频出错....");
    }

    @Override
    public R deleteBatch(List<String> videoIdList) {
        System.out.println("已触发服务降级....");
        return R.error().message("删除多个视频出错....");
    }
}
