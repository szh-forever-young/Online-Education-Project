package com.szh.eduservice.service;

import com.szh.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
public interface EduVideoService extends IService<EduVideo> {

    //根据课程id删除课程中的小节
    void removeVideoByCourseId(String courseId);

    //删除小节
    //同时调用service_vod中的删除视频接口，实现视频的删除
    void deleteVodByVideo(String id);
}
