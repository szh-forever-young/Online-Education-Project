package com.szh.eduservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 */
@Component
@FeignClient(name = "service-order",fallback = OrdersClientImpl.class)
public interface OrdersClient {

    //根据课程id、用户id查询订单记录表中的订单状态
    @GetMapping(value = "/eduorder/order/isBuyCourse/{courseId}/{memberId}")
    public boolean isBuyCourse(@PathVariable("courseId") String courseId,
                               @PathVariable("memberId") String memberId);
}
