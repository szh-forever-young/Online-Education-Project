package com.szh.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.szh.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.eduservice.entity.frontvo.CourseFrontVo;
import com.szh.eduservice.entity.frontvo.CourseWebVo;
import com.szh.eduservice.entity.vo.CourseInfoVo;
import com.szh.eduservice.entity.vo.CoursePublishVo;
import com.szh.eduservice.entity.vo.CourseQuery;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
public interface EduCourseService extends IService<EduCourse> {

    //添加课程基本信息
    String saveCourseInfo(CourseInfoVo courseInfoVo);

    //根据课程id查询课程基本信息
    CourseInfoVo getCourseInfo(String courseId);

    //修改课程信息
    void updateCourseInfo(CourseInfoVo courseInfoVo);

    //根据课程id查询课程的特定信息
    CoursePublishVo getPublishCourseInfo(String id);

    //删除课程
    void removeCourse(String courseId);

    //分页查询课程
    Map pageListCourse(long current, long limit);

    //条件查询（带分页）
    Map pageCourseCondition(long current, long limit, CourseQuery courseQuery);

    //查询前8条热门课程
    List<EduCourse> getCourseList();

    //条件查询带分页查询课程
    Map<String, Object> getFrontCourseList(Page<EduCourse> pageCourse, CourseFrontVo courseFrontVo);

    //根据课程id，编写sql语句查询课程信息
    CourseWebVo getBaseCourseInfo(String courseId);
}
