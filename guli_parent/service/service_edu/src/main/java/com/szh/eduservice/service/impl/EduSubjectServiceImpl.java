package com.szh.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.szh.eduservice.entity.EduSubject;
import com.szh.eduservice.entity.excel.SubjectData;
import com.szh.eduservice.entity.subject.OneSubject;
import com.szh.eduservice.entity.subject.TwoSubject;
import com.szh.eduservice.listener.SubjectExcelListener;
import com.szh.eduservice.mapper.EduSubjectMapper;
import com.szh.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-03
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    //添加课程分类
    @Override
    public void saveSubject(MultipartFile file,EduSubjectService subjectService) {
        try {
            //获取文件输入流
            InputStream in = file.getInputStream();
            //调用方法进行读取
            EasyExcel.read(in, SubjectData.class,new SubjectExcelListener(subjectService)).sheet().doRead();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //课程分类列表
    @Override
    public List<OneSubject> getAllOneTwoSubject() {
        //查询所有的一级分类 parentid = 0
        QueryWrapper<EduSubject> wrapperOne = new QueryWrapper<>();
        wrapperOne.eq("parent_id","0");
        wrapperOne.orderByAsc("sort", "id");
        List<EduSubject> oneSubjectList = baseMapper.selectList(wrapperOne);

        //查询所有的二级分类 parentid <> 0
        QueryWrapper<EduSubject> wrapperTwo = new QueryWrapper<>();
        wrapperTwo.ne("parent_id","0");
        wrapperTwo.orderByAsc("sort", "id");
        List<EduSubject> twoSubjectList = baseMapper.selectList(wrapperTwo);

        //创建List集合，用于存储最终的封装数据
        List<OneSubject> finalSubjectList = new ArrayList<>();

        //封装一级分类
        //遍历一级分类oneSubjectList集合
        for (int i = 0; i < oneSubjectList.size(); i++) {
            //得到oneSubjectList集合中的每个一级分类eduSubject对象
            EduSubject eduSubject = oneSubjectList.get(i);

            //将eduSubject对象中对应的值复制到oneSubject对象中
            OneSubject oneSubject = new OneSubject();
            BeanUtils.copyProperties(eduSubject,oneSubject);

            //将oneSubject对象存入finalSubjectList集合中
            finalSubjectList.add(oneSubject);

            //每遍历一个一级分类，就查询出这个一级分类下的所有二级分类
            List<TwoSubject> twoFinalSubjectList = new ArrayList<>();
            //遍历二级分类twoSubjectList集合
            for (int j = 0; j < twoSubjectList.size(); j++) {
                //得到twoSubjectList集合中的每个二级分类eduSubject对象
                EduSubject tSubject = twoSubjectList.get(j);
                //判断二级分类的parentid和一级分类的id是否一样
                if (tSubject.getParentId().equals(oneSubject.getId())) {
                    //将tSubject对象中对应的值复制到twoSubject对象中
                    TwoSubject twoSubject = new TwoSubject();
                    BeanUtils.copyProperties(tSubject,twoSubject);
                    //将二级分类twoSubject对象添加到twoFinalSubjectList集合中
                    twoFinalSubjectList.add(twoSubject);
                }
            }

            //将二级分类放到对应的一级分类中
            oneSubject.setChildren(twoFinalSubjectList);
        }

        return finalSubjectList;
    }
}
