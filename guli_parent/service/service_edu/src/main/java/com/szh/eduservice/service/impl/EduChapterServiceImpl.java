package com.szh.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.szh.eduservice.entity.EduChapter;
import com.szh.eduservice.entity.EduVideo;
import com.szh.eduservice.entity.chapter.ChapterVo;
import com.szh.eduservice.entity.chapter.VideoVo;
import com.szh.eduservice.mapper.EduChapterMapper;
import com.szh.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.eduservice.service.EduVideoService;
import com.szh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService videoService;

    //课程大纲列表，根据课程id先将对应课程查询出来
    @Override
    public List<ChapterVo> getChapterVideoByCourseId(String courseId) {
        //根据课程id查询课程中的所有章节
        QueryWrapper<EduChapter> wrapperChapter = new QueryWrapper<>();
        wrapperChapter.eq("course_id",courseId);
        List<EduChapter> eduChapterList = baseMapper.selectList(wrapperChapter);

        //根据课程id查询课程章节中的所有小节
        QueryWrapper<EduVideo> wrapperVideo = new QueryWrapper<>();
        wrapperVideo.eq("course_id",courseId);
        List<EduVideo> eduVideoList = videoService.list(wrapperVideo);

        //创建list集合，用于封装最终的数据
        List<ChapterVo> finalList = new ArrayList<>();

        //遍历查询的章节list集合，进行封装
        for (int i = 0; i < eduChapterList.size(); i++) {
            //得到课程中的每个章节
            EduChapter eduChapter = eduChapterList.get(i);
            //将eduChapter对象中的值复制到chapterVo中
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(eduChapter,chapterVo);
            //将chapterVo对象存入finalList集合中
            finalList.add(chapterVo);

            //创建集合，用于封装当前课程章节中的所有小节
            List<VideoVo> videoVoList = new ArrayList<>();

            //遍历查询的小节list集合，进行封装
            for (int j = 0; j < eduVideoList.size(); j++) {
                //得到课程章节中的每个小节
                EduVideo eduVideo = eduVideoList.get(j);
                //判断小节中的章节id（chapterId）和章节id是否一样
                if (eduVideo.getChapterId().equals(eduChapter.getId())) {
                    //将eduVideo对象中的值复制到videoVo中
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(eduVideo,videoVo);
                    //将小节对象videoVo存入videoVoList集合中
                    videoVoList.add(videoVo);
                }
            }

            //把封装好的小节集合videoVoList放到章节对象中
            chapterVo.setChildren(videoVoList);
        }

        return finalList;
    }

    //删除章节
    @Override
    public boolean deleteChapter(String chapterId) {
        //根据章节id查询其中是否有小节
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id",chapterId);
        //如果章节下面有小节，则count > 0；反之则等于0
        int count = videoService.count(wrapper);
        if (count > 0) { //有小节，则抛异常，不执行删除
            throw new GuliException(20001,"不能删除");
        } else { //没有小节，执行删除
            int result = baseMapper.deleteById(chapterId);
            return result > 0;
        }
    }

    //根据课程id删除课程中的章节
    @Override
    public void removeChapterByCourseId(String courseId) {
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);
    }
}
