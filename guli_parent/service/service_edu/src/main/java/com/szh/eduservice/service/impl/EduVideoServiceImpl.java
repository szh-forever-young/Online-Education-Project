package com.szh.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.szh.commonutils.R;
import com.szh.eduservice.client.VodClient;
import com.szh.eduservice.entity.EduVideo;
import com.szh.eduservice.mapper.EduVideoMapper;
import com.szh.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-04
 */
@Service
@DefaultProperties(defaultFallback = "globalFallbackMethod")
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VodClient vodClient;

    //根据课程id删除课程中的小节
    @Override
//    @HystrixCommand(fallbackMethod = "deleteCourseFallbackMethod",
//                    commandProperties = {
//                            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="1500")
//                    })
    @HystrixCommand
    public void removeVideoByCourseId(String courseId) {
        //根据课程id查询出课程中的所有视频id，封装到eduVideoList集合中
        QueryWrapper<EduVideo> wrapperVideo = new QueryWrapper<>();
        wrapperVideo.eq("course_id",courseId);
        wrapperVideo.select("video_source_id");
        List<EduVideo> eduVideoList = baseMapper.selectList(wrapperVideo);

        //将eduVideoList集合中的数据进行遍历，修改为只保留videoId的格式
        List<String> videoIdList = new ArrayList<>();
        for (int i = 0; i < eduVideoList.size(); i++) {
            EduVideo eduVideo = eduVideoList.get(i);
            String videoSourceId = eduVideo.getVideoSourceId();
            if (!StringUtils.isEmpty(videoSourceId)) {
                videoIdList.add(videoSourceId);
            }
        }
        if (videoIdList.size() > 0) {
            vodClient.deleteBatch(videoIdList); //根据多个视频id进行视频的删除
        }

        //最后将对应的课程删除
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);
    }
//    public void deleteCourseFallbackMethod(String courseId) {
//        System.out.println("删除视频失败, 请稍后再试..../(ㄒoㄒ)/~~");
//    }

    //删除小节
    //同时调用service_vod中的删除视频接口，实现视频的删除
    @Override
//    @HystrixCommand(fallbackMethod = "deleteVideoFallbackMethod",
//                    commandProperties = {
//                            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="1500")
//                    })
    @HystrixCommand
    public void deleteVodByVideo(String id) {
        //根据小节id获取视频id
        EduVideo eduVideo = baseMapper.selectById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
        //判断该小节中是否存在视频 (存在再执行删除视频的操作)
        if (!StringUtils.isEmpty(videoSourceId)) {
            //同时调用service_vod中的删除视频接口，实现视频的删除
            R result = vodClient.removeAlyVideo(videoSourceId); //先删除小节中的视频
            if (result.getCode() == 20001) {
                throw new GuliException(20001,"删除视频出问题啦!!!");
            } else {
                baseMapper.deleteById(id); //删除小节本身
            }
        } else {
            baseMapper.deleteById(id); //删除小节本身
        }
    }
//    public void deleteVideoFallbackMethod(String id) {
//        System.out.println("删除视频失败, 请稍后再试..../(ㄒoㄒ)/~~");
//    }
    public void globalFallbackMethod() {
        System.out.println("删除视频失败, 请稍后再试..../(ㄒoㄒ)/~~");
    }

}
