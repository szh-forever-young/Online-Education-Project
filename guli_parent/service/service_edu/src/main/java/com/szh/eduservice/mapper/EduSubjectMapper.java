package com.szh.eduservice.mapper;

import com.szh.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-03
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
