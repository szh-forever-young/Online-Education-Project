package com.szh.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.szh.eduservice.entity.EduTeacher;
import com.szh.eduservice.entity.vo.TeacherQuery;
import com.szh.eduservice.mapper.EduTeacherMapper;
import com.szh.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-08-30
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

    //分页查询讲师
    @Override
    public Map pageTeacher(long current, long limit) {
        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);
        baseMapper.selectPage(pageTeacher,null);

        long total = pageTeacher.getTotal(); //总记录数
        List<EduTeacher> records = pageTeacher.getRecords(); //所有数据集

        Map map = new HashMap();
        map.put("total",total);
        map.put("rows",records);
        return map;
    }

    //条件查询（附带分页）
    @Override
    public Map pageTeacherCondition(long current, long limit, TeacherQuery teacherQuery) {
        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);

        //构建查询条件对象
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();

        //动态sql语句的拼接
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name",name);
        }
        if (!StringUtils.isEmpty(level)) {
            wrapper.eq("level",level);
        }
        if (!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create",begin);
        }
        if (!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create",end);
        }
        wrapper.orderByDesc("gmt_create");

        //调用方法实现条件查询分页显示
        baseMapper.selectPage(pageTeacher,wrapper);

        //将总记录数、数据集封装到map集合中返回
        long total = pageTeacher.getTotal(); //总记录数
        List<EduTeacher> records = pageTeacher.getRecords(); //所有数据集
        Map map = new HashMap();
        map.put("total",total);
        map.put("rows",records);
        return map;
    }

    //查询前4位名师
    @Override
    @Cacheable(key = "'selectTeacher'",value = "teacher")
    public List<EduTeacher> getTeacherList() {
        QueryWrapper<EduTeacher> wrapperTeacher = new QueryWrapper<>();
        wrapperTeacher.orderByDesc("id");
        wrapperTeacher.last("limit 4");
        List<EduTeacher> teacherList = baseMapper.selectList(wrapperTeacher);
        return teacherList;
    }

    //分页查询讲师
    @Override
    public Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageTeacher) {
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        baseMapper.selectPage(pageTeacher,wrapper);

        //获取所有分页相关的数据
        List<EduTeacher> records = pageTeacher.getRecords(); //分页数据集合
        long current = pageTeacher.getCurrent(); //当前页
        long pages = pageTeacher.getPages(); //总页数
        long size = pageTeacher.getSize(); //每页记录数
        long total = pageTeacher.getTotal(); //总记录数
        boolean hasNext = pageTeacher.hasNext(); //是否有下一页
        boolean hasPrevious = pageTeacher.hasPrevious(); //是否有上一页

        //将分页的数据封装到map集合中, 最后返回map集合
        Map<String,Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        return map;
    }

}
