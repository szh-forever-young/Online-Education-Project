package com.szh.eduservice.client;

import com.szh.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * OpenFeign服务接口调用
 * service_edu作为服务消费者, service_vod作为服务提供者
 * service_edu调用service_vod中的删除视频接口，实现视频的删除
 */
@Component
@FeignClient(value = "service-vod",fallback = VodFileDegradeFeignClient.class)
public interface VodClient {

    //远程调用service_vod中的删除视频接口 (删除小节同时删除视频)
    //@PathVariable注解一定要指定参数名称，否则出错
    @DeleteMapping(value = "/eduvod/video/removeAlyVideo/{id}")
    public R removeAlyVideo(@PathVariable("id") String id);

    //远程调用service_vod中的删除多个阿里云视频接口 (删除课程同时删除视频)
    @DeleteMapping(value = "/eduvod/video/delete-batch")
    public R deleteBatch(@RequestParam("videoIdList") List<String> videoIdList);

}
