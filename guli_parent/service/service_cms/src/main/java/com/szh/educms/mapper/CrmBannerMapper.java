package com.szh.educms.mapper;

import com.szh.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-10
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
