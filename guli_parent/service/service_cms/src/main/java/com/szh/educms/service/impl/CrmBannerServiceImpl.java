package com.szh.educms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.szh.educms.entity.CrmBanner;
import com.szh.educms.mapper.CrmBannerMapper;
import com.szh.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-10
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    //分页查询banner
    @Override
    public Map pageBanner(long page, long limit) {
        Page<CrmBanner> pageBanner = new Page<>(page,limit);
        baseMapper.selectPage(pageBanner,null);
        long total = pageBanner.getTotal(); //总记录数
        List<CrmBanner> records = pageBanner.getRecords(); //所有数据集

        Map map = new HashMap();
        map.put("total",total);
        map.put("item",records);
        return map;
    }

    //查询所有banner
    @Override
    @Cacheable(key = "'selectIndexList'",value = "banner")
    public List<CrmBanner> selectAllBanner() {
        //先根据banner的id进行降序排列，只显示前2条记录
        QueryWrapper<CrmBanner> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        //调用last方法进行sql语句拼接
        wrapper.last("limit 2");

        List<CrmBanner> list = baseMapper.selectList(wrapper);
        return list;
    }
}
