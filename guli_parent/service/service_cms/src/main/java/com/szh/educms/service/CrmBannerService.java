package com.szh.educms.service;

import com.szh.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-10
 */
public interface CrmBannerService extends IService<CrmBanner> {

    //分页查询banner
    Map pageBanner(long page, long limit);

    //查询所有banner
    List<CrmBanner> selectAllBanner();
}
