package com.szh.educms.controller;

import com.szh.commonutils.R;
import com.szh.educms.entity.CrmBanner;
import com.szh.educms.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 *
 */
@RestController
@RequestMapping("/educms/banneradmin")
//@CrossOrigin
public class BannerAdminController {

    @Autowired
    private CrmBannerService bannerService;

    //分页查询banner
    @GetMapping(value = "/pageBanner/{page}/{limit}")
    public R pageBanner(@PathVariable long page,
                        @PathVariable long limit) {
        Map map = bannerService.pageBanner(page,limit);
        return R.ok().data(map);
    }

    //添加banner
    @PostMapping(value = "/addBanner")
    public R addBanner(@RequestBody CrmBanner crmBanner) {
        bannerService.save(crmBanner);
        return R.ok();
    }

    //根据id查询banner
    @GetMapping(value = "/get/{id}")
    public R getBanner(@PathVariable String id) {
        CrmBanner banner = bannerService.getById(id);
        return R.ok().data("item",banner);
    }

    //修改banner
    @PutMapping(value = "/save")
    public R saveBanner(@RequestBody CrmBanner crmBanner) {
        bannerService.updateById(crmBanner);
        return R.ok();
    }

    //删除banner
    @DeleteMapping(value = "/remove/{id}")
    public R removeBanner(@PathVariable String id) {
        bannerService.removeById(id);
        return R.ok();
    }

}
