package com.szh.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.szh.commonutils.JwtUtils;
import com.szh.commonutils.MD5;
import com.szh.educenter.entity.UcenterMember;
import com.szh.educenter.entity.vo.RegisterVo;
import com.szh.educenter.mapper.UcenterMemberMapper;
import com.szh.educenter.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-11
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    //登录
    @Override
    public String login(UcenterMember member) {
        //获取登录手机号和密码
        String mobile = member.getMobile();
        String password = member.getPassword();

        //手机号和密码进行非空判断
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) {
            throw new GuliException(20001,"登录失败....");
        }

        //判断手机号是否正确
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        UcenterMember mobileMember = baseMapper.selectOne(wrapper);
        //判断查询对象mobileMember是否为空，为空表示数据库中没有该用户信息
        if (mobileMember == null) {
            throw new GuliException(20001,"登录失败....");
        }

        //mobileMember不为空, 再进行密码的判断
        //因为存储到数据库密码肯定加密的, 把输入的密码进行加密, 再和数据库密码进行比较
        //加密方式 MD5
        if (!MD5.encrypt(password).equals(mobileMember.getPassword())) {
            throw new GuliException(20001,"登录失败....");
        }

        //判断用户是否被禁用
        if (mobileMember.getIsDisabled()) {
            throw new GuliException(20001,"登录失败....");
        }

        //程序走到这里，则表示登录成功
        //使用JWT工具类生成token字符串
        String token = JwtUtils.getJwtToken(mobileMember.getId(),mobileMember.getNickname());

        return token;
    }

    //注册
    @Override
    public void register(RegisterVo registerVo) {
        //获取注册的数据
        String nickname = registerVo.getNickname(); //昵称
        String mobile = registerVo.getMobile();     //手机号
        String password = registerVo.getPassword(); //密码
        String code = registerVo.getCode();         //验证码

        //非空判断
        if (StringUtils.isEmpty(nickname) || StringUtils.isEmpty(mobile)
                || StringUtils.isEmpty(password) || StringUtils.isEmpty(code)) {
            throw new GuliException(20001,"注册失败....");
        }

        //判断验证码和redis中的是否一致
        String redisCode = redisTemplate.opsForValue().get(mobile);
        if (!code.equals(redisCode)) {
            throw new GuliException(20001,"注册失败....");
        }

        //判断手机号是否重复，重复的手机号无法再次注册
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        //查询该手机号在数据库中是否存在
        Integer count = baseMapper.selectCount(wrapper);
        if (count > 0) {
            throw new GuliException(20001,"注册失败....");
        }

        //走到这一步，说明该手机号在数据库中不存在，各个数据不为空，验证码正确
        //注册成功，将数据添加到数据库中
        UcenterMember member = new UcenterMember();
        //依次存入手机号、昵称、加密后的密码、用户不禁用、头像信息
        member.setMobile(mobile);
        member.setNickname(nickname);
        member.setPassword(MD5.encrypt(password));
        member.setIsDisabled(false);
        member.setAvatar("https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/03/7075de64b31045639b71144abce35e6dfile.png");
        baseMapper.insert(member);
    }

    //根据openid判断
    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid",openid);
        UcenterMember member = baseMapper.selectOne(wrapper);
        return member;
    }

    //查询某一天的注册人数
    @Override
    public Integer countRegister(String day) {
        return baseMapper.countRegister(day);
    }

}
