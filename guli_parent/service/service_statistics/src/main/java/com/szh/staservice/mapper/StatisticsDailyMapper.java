package com.szh.staservice.mapper;

import com.szh.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-15
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
