/*
Navicat MySQL Data Transfer

Source Server         : MyProject
Source Server Version : 50605
Source Host           : localhost:3306
Source Database       : gulixueyuan

Target Server Type    : MYSQL
Target Server Version : 50605
File Encoding         : 65001

Date: 2021-09-21 14:43:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for edu_chapter
-- ----------------------------
DROP TABLE IF EXISTS `edu_chapter`;
CREATE TABLE `edu_chapter` (
  `id` char(19) NOT NULL COMMENT '章节ID',
  `course_id` char(19) NOT NULL COMMENT '课程ID',
  `title` varchar(50) NOT NULL COMMENT '章节名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '显示排序',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程';

-- ----------------------------
-- Records of edu_chapter
-- ----------------------------
INSERT INTO `edu_chapter` VALUES ('1', '14', '第一章：HTML', '0', '2021-09-21 11:31:32', '2021-09-21 11:31:55');
INSERT INTO `edu_chapter` VALUES ('1192252428399751169', '1192252213659774977', '第一章节', '0', '2021-09-21 11:31:36', '2021-09-21 11:31:58');
INSERT INTO `edu_chapter` VALUES ('1434419880228143106', '1434419802016956417', '第一章 测试1', '1', '2021-09-05 15:35:24', '2021-09-05 16:11:18');
INSERT INTO `edu_chapter` VALUES ('1434421211277934593', '1434419802016956417', '第二章 测试2', '2', '2021-09-05 15:40:42', '2021-09-05 15:40:42');
INSERT INTO `edu_chapter` VALUES ('1434459558092337154', '1434419802016956417', '第三章 测试3', '3', '2021-09-05 18:13:04', '2021-09-05 18:13:04');
INSERT INTO `edu_chapter` VALUES ('1439845994580774913', '18', 'Java中的继承', '0', '2021-09-20 14:56:51', '2021-09-20 14:56:51');
INSERT INTO `edu_chapter` VALUES ('20', '18', 'Java集合', '0', '2021-09-21 11:31:45', '2021-09-17 12:56:48');
INSERT INTO `edu_chapter` VALUES ('3', '14', '第二章：CSS', '0', '2021-09-21 11:31:48', '2021-09-21 11:32:03');
INSERT INTO `edu_chapter` VALUES ('30', '18', 'Java常用API', '0', '2021-09-21 11:31:52', '2021-09-21 11:32:06');

-- ----------------------------
-- Table structure for edu_course
-- ----------------------------
DROP TABLE IF EXISTS `edu_course`;
CREATE TABLE `edu_course` (
  `id` char(19) NOT NULL COMMENT '课程ID',
  `teacher_id` char(19) NOT NULL COMMENT '课程讲师ID',
  `subject_id` char(19) NOT NULL COMMENT '课程专业ID',
  `subject_parent_id` char(19) DEFAULT NULL COMMENT '课程专业父级ID',
  `title` varchar(50) NOT NULL COMMENT '课程标题',
  `price` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '课程销售价格，设置为0则可免费观看',
  `lesson_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '总课时',
  `cover` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '课程封面图片路径',
  `buy_count` bigint(10) unsigned NOT NULL DEFAULT '0' COMMENT '销售数量',
  `view_count` bigint(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览数量',
  `version` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `status` varchar(10) NOT NULL DEFAULT 'Draft' COMMENT '课程状态 Draft未发布  Normal已发布',
  `is_deleted` tinyint(3) DEFAULT NULL COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_title` (`title`),
  KEY `idx_subject_id` (`subject_id`),
  KEY `idx_teacher_id` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程';

-- ----------------------------
-- Records of edu_course
-- ----------------------------
INSERT INTO `edu_course` VALUES ('1', '12', '1433984844743835650', '1433984844743835649', '一号课程', '0.01', '0', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/30.png', '0', '100', '1', 'Normal', null, '2021-09-06 14:46:04', '2021-09-21 13:49:59');
INSERT INTO `edu_course` VALUES ('1192252213659774977', '6', '1433984844676726786', '1433984844630589441', 'vue基本语法', '0.01', '2', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/32.png', '4', '387', '1', 'Normal', '0', '2021-09-21 11:28:14', '2021-09-21 11:28:17');
INSERT INTO `edu_course` VALUES ('14', '7', '1433984844806750210', '1433984844806750209', 'MySQL从删库到跑路', '0.00', '3', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/34.png', '3', '685', '15', 'Normal', '0', '2021-09-21 11:28:21', '2021-09-21 11:28:24');
INSERT INTO `edu_course` VALUES ('1434348307932557314', '11', '1433984844743835650', '1433984844743835649', '熔岩巨兽玩法教程', '0.01', '48', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/36.png', '0', '333', '1', 'Normal', null, '2021-09-05 10:51:00', '2021-09-21 13:50:07');
INSERT INTO `edu_course` VALUES ('1434419802016956417', '12', '1433984844676726787', '1433984844630589441', 'test666', '0.01', '10', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/38.png', '0', '1', '1', 'Normal', null, '2021-09-05 15:35:06', '2021-09-06 10:32:20');
INSERT INTO `edu_course` VALUES ('1438724548634710017', '11', '1433984844743835651', '1433984844743835649', '没用的课', '0.00', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/40.png', '0', '5', '1', 'Normal', null, '2021-09-17 12:40:37', '2021-09-17 12:40:40');
INSERT INTO `edu_course` VALUES ('15', '10', '1433984844806750210', '1433984844806750209', 'MySQL高级/优化', '0.00', '23', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/39.png', '0', '520', '17', 'Normal', '0', '2021-09-21 11:28:33', '2021-09-21 11:28:37');
INSERT INTO `edu_course` VALUES ('18', '8', '1433984844743835650', '1433984844743835649', 'Java精品课程', '0.01', '20', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/37.png', '151', '737', '6', 'Normal', '0', '2021-09-21 11:28:28', '2021-09-21 11:28:40');
INSERT INTO `edu_course` VALUES ('2', '10', '1433984844743835650', '1433984844743835649', '二号课程', '0.00', '11', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/35.png', '0', '99', '1', 'Normal', null, '2021-09-06 14:47:25', '2021-09-21 13:49:54');
INSERT INTO `edu_course` VALUES ('3', '9', '1433984844743835650', '1433984844743835649', '三号课程', '0.00', '22', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/33.png', '0', '12', '1', 'Normal', null, '2021-09-06 14:48:07', '2021-09-21 13:49:49');
INSERT INTO `edu_course` VALUES ('4', '8', '1433984844743835650', '1433984844743835649', '四号课程', '0.00', '33', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/photos/31.png', '0', '234', '1', 'Normal', null, '2021-09-06 14:48:51', '2021-09-21 13:49:42');

-- ----------------------------
-- Table structure for edu_course_description
-- ----------------------------
DROP TABLE IF EXISTS `edu_course_description`;
CREATE TABLE `edu_course_description` (
  `id` char(19) NOT NULL COMMENT '课程ID',
  `description` text COMMENT '课程简介',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='课程简介';

-- ----------------------------
-- Records of edu_course_description
-- ----------------------------
INSERT INTO `edu_course_description` VALUES ('1104870479077879809', '只要学不死，就往死里学。', '2021-09-21 11:27:47', '2021-09-21 11:27:50');
INSERT INTO `edu_course_description` VALUES ('1192252213659774977', '就这？就这？就这？？？', '2021-09-21 11:28:02', '2021-09-21 11:28:05');
INSERT INTO `edu_course_description` VALUES ('1434348307932557314', '俺也是从石头缝里蹦出来的，为啥不是猴子呢？', '2021-09-05 10:51:00', '2021-09-05 10:51:00');
INSERT INTO `edu_course_description` VALUES ('1434419802016956417', '学吧，肝就完事了。。。', '2021-09-05 15:35:06', '2021-09-05 15:50:39');
INSERT INTO `edu_course_description` VALUES ('1438724548634710017', '没用的课程才最有用！！！', '2021-09-17 12:40:37', '2021-09-17 12:40:37');
INSERT INTO `edu_course_description` VALUES ('18', '本套Java视频完全针对零基础学员，课堂实录，自发布以来，好评如潮！Java视频中注重与学生互动，讲授幽默诙谐、细致入微，覆盖Java基础所有核心知识点，同类Java视频中也是代码量大、案例多、实战性强的。同时，本Java视频教程注重技术原理剖析，深入JDK源码，辅以代码实战贯穿始终，用实践驱动理论，并辅以必要的代码练习。</p>\n<p>------------------------------------</p>\n<p>视频特点：</p>\n<p>通过学习本Java视频教程，大家能够真正将Java基础知识学以致用、活学活用，构架Java编程思想，牢牢掌握\"源码级\"的Javase核心技术，并为后续JavaWeb等技术的学习奠定扎实基础。<br /><br />1.通俗易懂，细致入微：每个知识点高屋建瓴，深入浅出，简洁明了的说明问题<br />2.具实战性：全程真正代码实战，涵盖上百个企业应用案例及练习<br />3.深入：源码分析，更有 Java 反射、动态代理的实际应用等<br />4.登录尚硅谷官网，技术讲师免费在线答疑', '2021-09-21 11:27:55', '2021-09-21 11:27:58');
INSERT INTO `edu_course_description` VALUES ('2', '这是第二号课程', '2021-09-06 15:40:36', '2021-09-06 15:40:39');
INSERT INTO `edu_course_description` VALUES ('3', '这是第三号课程', '2021-09-06 15:40:55', '2021-09-06 15:40:57');
INSERT INTO `edu_course_description` VALUES ('4', '这是第四号课程', '2021-09-06 15:41:07', '2021-09-06 15:41:09');

-- ----------------------------
-- Table structure for edu_subject
-- ----------------------------
DROP TABLE IF EXISTS `edu_subject`;
CREATE TABLE `edu_subject` (
  `id` char(19) NOT NULL COMMENT '课程类别ID',
  `title` varchar(10) NOT NULL COMMENT '类别名称',
  `parent_id` char(19) NOT NULL DEFAULT '0' COMMENT '父ID',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程科目';

-- ----------------------------
-- Records of edu_subject
-- ----------------------------
INSERT INTO `edu_subject` VALUES ('1433984844630589441', '前端开发', '0', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844676726786', 'vue', '1433984844630589441', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844676726787', 'JavaScript', '1433984844630589441', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844676726788', 'jQuery', '1433984844630589441', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844743835649', '后端开发', '0', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844743835650', 'java', '1433984844743835649', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844743835651', 'c++', '1433984844743835649', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844806750209', '数据库开发', '0', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');
INSERT INTO `edu_subject` VALUES ('1433984844806750210', 'mysql', '1433984844806750209', '0', '2021-09-04 10:46:44', '2021-09-04 10:46:44');

-- ----------------------------
-- Table structure for edu_teacher
-- ----------------------------
DROP TABLE IF EXISTS `edu_teacher`;
CREATE TABLE `edu_teacher` (
  `id` char(19) NOT NULL COMMENT '讲师ID',
  `name` varchar(20) NOT NULL COMMENT '讲师姓名',
  `intro` varchar(500) NOT NULL DEFAULT '' COMMENT '讲师简介',
  `career` varchar(500) DEFAULT NULL COMMENT '讲师资历,一句话说明讲师',
  `level` int(10) unsigned NOT NULL COMMENT '头衔 1高级讲师 2首席讲师',
  `avatar` varchar(255) DEFAULT NULL COMMENT '讲师头像',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='讲师';

-- ----------------------------
-- Records of edu_teacher
-- ----------------------------
INSERT INTO `edu_teacher` VALUES ('1', '光头强', '只会砍树', '伐木工', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/6.jpg', '1', '0', '2021-09-17 12:41:49', '2021-09-17 12:41:49');
INSERT INTO `edu_teacher` VALUES ('10', '张起灵', '999', '999', '2', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/3.jpg', '0', '0', '2021-08-30 15:34:32', '2021-09-02 15:27:07');
INSERT INTO `edu_teacher` VALUES ('11', '小哥', '玩转Java编程的盗墓者', '有点水', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/1.jpg', '1', '0', '2021-09-21 11:14:16', '2021-09-21 11:14:21');
INSERT INTO `edu_teacher` VALUES ('12', '小宋', '集中起来的意志可以击穿顽石', '牛', '2', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/5.jpg', '0', '0', '2021-09-21 11:14:38', '2021-09-21 11:14:43');
INSERT INTO `edu_teacher` VALUES ('2', '疾风剑豪', '面对......疾风吧', '升E行吗', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/4.jpg', '1', '0', '2021-09-03 14:18:12', '2021-09-03 14:18:12');
INSERT INTO `edu_teacher` VALUES ('3', '狂战士', '狱血魔神', '帝血弑天', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/8.jpg', '0', '0', '2021-09-21 11:14:51', '2021-09-21 11:14:54');
INSERT INTO `edu_teacher` VALUES ('4', '关云长', '武圣', '青龙偃月刀', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/9.jpg', '0', '0', '2021-09-21 11:14:58', '2021-09-21 11:15:02');
INSERT INTO `edu_teacher` VALUES ('5', '张翼德', '五虎上将之一', '丈八蛇矛', '2', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/10.jpg', '0', '0', '2021-09-21 11:15:21', '2021-09-21 11:15:24');
INSERT INTO `edu_teacher` VALUES ('6', '卧龙', '诸葛孔明', '丞相', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/11.jpg', '0', '0', '2021-09-21 11:15:13', '2021-09-21 11:15:17');
INSERT INTO `edu_teacher` VALUES ('7', '凤雏', '庞士元', '军师', '2', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/12.jpg', '2', '0', '2021-09-21 11:15:06', '2021-09-21 11:15:09');
INSERT INTO `edu_teacher` VALUES ('8', '闷油瓶', '从不说话的讲师', '哑巴张', '1', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/7.jpg', '0', '0', '2021-09-02 13:44:22', '2021-09-02 13:44:24');
INSERT INTO `edu_teacher` VALUES ('9', '冷少', '外冷内热', '熔岩巨兽专业操纵者', '2', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/21/photos/2.jpg', '1', '0', '2021-09-02 14:48:22', '2021-09-02 14:48:22');

-- ----------------------------
-- Table structure for edu_video
-- ----------------------------
DROP TABLE IF EXISTS `edu_video`;
CREATE TABLE `edu_video` (
  `id` char(19) NOT NULL COMMENT '视频ID',
  `course_id` char(19) NOT NULL COMMENT '课程ID',
  `chapter_id` char(19) NOT NULL COMMENT '章节ID',
  `title` varchar(50) NOT NULL COMMENT '节点名称',
  `video_source_id` varchar(100) DEFAULT NULL COMMENT '云端视频资源',
  `video_original_name` varchar(100) DEFAULT NULL COMMENT '原始文件名称',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序字段',
  `play_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '播放次数',
  `is_free` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否可以试听：0收费 1免费',
  `duration` float NOT NULL DEFAULT '0' COMMENT '视频时长（秒）',
  `status` varchar(20) NOT NULL DEFAULT 'Empty' COMMENT 'Empty未上传 Transcoding转码中  Normal正常',
  `size` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '视频源文件大小（字节）',
  `version` bigint(20) unsigned NOT NULL DEFAULT '1' COMMENT '乐观锁',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_chapter_id` (`chapter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT COMMENT='课程视频';

-- ----------------------------
-- Records of edu_video
-- ----------------------------
INSERT INTO `edu_video` VALUES ('1189476403626409986', '18', '1181729226915577857', '22', '5155c73dc112475cbbddccf4723f7cef', '视频.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2021-09-21 11:33:05', '2021-09-21 11:33:08');
INSERT INTO `edu_video` VALUES ('1192252824606289921', '1192252213659774977', '1192252428399751169', '第一课时', '756cf06db9cb4f30be85a9758b19c645', 'eae2b847ef8503b81f5d5593d769dde2.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2021-09-21 11:33:00', '2021-09-21 11:33:11');
INSERT INTO `edu_video` VALUES ('1192628092797730818', '1192252213659774977', '1192252428399751169', '第二课时', '2a02d726622f4c7089d44cb993c531e1', 'eae2b847ef8503b81f5d5593d769dde2.mp4', '0', '0', '1', '0', 'Empty', '0', '1', '2021-09-21 11:32:56', '2021-09-21 11:33:14');
INSERT INTO `edu_video` VALUES ('1192632495013380097', '1192252213659774977', '1192252428399751169', '第三课时', '4e560c892fdf4fa2b42e0671aa42fa9d', 'eae2b847ef8503b81f5d5593d769dde2.mp4', '0', '0', '1', '0', 'Empty', '0', '1', '2021-09-21 11:32:53', '2021-09-21 11:33:18');
INSERT INTO `edu_video` VALUES ('1194117638832111617', '1192252213659774977', '1192252428399751169', '第四课时', '4e560c892fdf4fa2b42e0671aa42fa9d', 'eae2b847ef8503b81f5d5593d769dde2.mp4', '0', '0', '0', '0', 'Empty', '0', '1', '2021-09-21 11:32:50', '2021-09-21 11:33:21');
INSERT INTO `edu_video` VALUES ('1196263770832023554', '1192252213659774977', '1192252428399751169', '第五课时', '27d21158b0834cb5a8d50710937de330', 'eae2b847ef8503b81f5d5593d769dde2.mp4', '5', '0', '0', '0', 'Empty', '0', '1', '2021-09-21 11:32:46', '2021-09-21 11:33:24');
INSERT INTO `edu_video` VALUES ('1434756599469060097', '18', '30', 'StringBuilder', '', null, '0', '0', '0', '0', 'Empty', '0', '1', '2021-09-06 13:53:24', '2021-09-06 13:53:24');
INSERT INTO `edu_video` VALUES ('1439846420759810050', '18', '1439845994580774913', 'extends关键字', '412168f822554e3f9d8ac92817662258', '6 - What If I Want to Move Faster.mp4', '1', '0', '1', '0', 'Empty', '0', '1', '2021-09-20 14:58:32', '2021-09-20 15:11:02');
INSERT INTO `edu_video` VALUES ('17', '18', '20', 'ArrayList', '196116a6fee742e1ba9f6c18f65bd8c1', '1', '2', '1000', '1', '100', 'Draft', '0', '1', '2021-09-21 11:32:37', '2021-09-21 11:32:41');
INSERT INTO `edu_video` VALUES ('18', '18', '20', 'HashMap', '2d99b08ca0214909899910c9ba042d47', '7 - How Do I Find Time for My ', '2', '999', '1', '100', 'Draft', '0', '1', '2021-09-21 11:32:27', '2021-09-21 11:32:33');
INSERT INTO `edu_video` VALUES ('20', '18', '30', 'String类', '2a38988892d84df598752226c50f3fa3', '00-day10总结.avi', '4', '666', '0', '100', 'Draft', '0', '1', '2021-09-21 11:32:20', '2021-09-21 11:32:23');
