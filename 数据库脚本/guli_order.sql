/*
Navicat MySQL Data Transfer

Source Server         : MyProject
Source Server Version : 50605
Source Host           : localhost:3306
Source Database       : gulixueyuan

Target Server Type    : MYSQL
Target Server Version : 50605
File Encoding         : 65001

Date: 2021-09-21 14:43:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` char(19) NOT NULL DEFAULT '',
  `order_no` varchar(32) NOT NULL DEFAULT '' COMMENT '订单号',
  `course_id` varchar(19) NOT NULL DEFAULT '' COMMENT '课程id',
  `course_title` varchar(100) DEFAULT NULL COMMENT '课程名称',
  `course_cover` varchar(255) DEFAULT NULL COMMENT '课程封面',
  `teacher_name` varchar(20) DEFAULT NULL COMMENT '讲师名称',
  `member_id` varchar(19) NOT NULL DEFAULT '' COMMENT '会员id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '会员昵称',
  `mobile` varchar(11) DEFAULT NULL COMMENT '会员手机',
  `total_fee` decimal(10,2) DEFAULT '0.01' COMMENT '订单金额（分）',
  `pay_type` tinyint(3) DEFAULT NULL COMMENT '支付类型（1：微信 2：支付宝）',
  `status` tinyint(3) DEFAULT NULL COMMENT '订单状态（0：未支付 1：已支付）',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_order_no` (`order_no`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='订单';

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('1437978849143963649', '20210915111728347', '1434348307932557314', '熔岩巨兽玩法教程', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/fa546369203840faa9d6cfeaad9591ea8f80790d-d736-4842-a6a4-4dcb0d684d4e.jpg', '小宋', '1436582068313141250', '张起灵', '13101792642', '0.01', '1', '1', '0', '2021-09-15 11:17:29', '2021-09-15 11:17:44');
INSERT INTO `t_order` VALUES ('1439823388464996354', '20210920132701728', '15', null, null, null, '1436582068313141250', '张起灵', '13101792642', '0.01', '1', '0', '0', '2021-09-20 13:27:01', '2021-09-20 13:27:01');
INSERT INTO `t_order` VALUES ('1439841877485510658', '20210920144029362', '18', 'Java精品课程', 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/10/fa546369203840faa9d6cfeaad9591ea8f80790d-d736-4842-a6a4-4dcb0d684d4e.jpg', '晴天', '1436582068313141250', '张起灵', '13101792642', '0.01', '1', '1', '0', '2021-09-20 14:40:29', '2021-09-20 14:40:56');

-- ----------------------------
-- Table structure for t_pay_log
-- ----------------------------
DROP TABLE IF EXISTS `t_pay_log`;
CREATE TABLE `t_pay_log` (
  `id` char(19) NOT NULL DEFAULT '',
  `order_no` varchar(32) NOT NULL DEFAULT '' COMMENT '订单号',
  `pay_time` datetime DEFAULT NULL COMMENT '支付完成时间',
  `total_fee` decimal(10,2) DEFAULT '0.01' COMMENT '支付金额（分）',
  `transaction_id` varchar(30) DEFAULT NULL COMMENT '交易流水号',
  `trade_state` char(20) DEFAULT NULL COMMENT '交易状态',
  `pay_type` tinyint(3) NOT NULL DEFAULT '0' COMMENT '支付类型（1：微信 2：支付宝）',
  `attr` text COMMENT '其他属性',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_order_no` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='支付日志表';

-- ----------------------------
-- Records of t_pay_log
-- ----------------------------
INSERT INTO `t_pay_log` VALUES ('1437978911622316033', '20210915111728347', '2021-09-15 11:17:44', '0.01', '4200001148202109152523472507', 'SUCCESS', '1', '{\"transaction_id\":\"4200001148202109152523472507\",\"nonce_str\":\"yhKYzk28iQ81C8nm\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuEpzOjI4QipIoXnrOLYx8dE\",\"sign\":\"7B1430B392D4807388975585C5DA5D50\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20210915111728347\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20210915111742\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}', '0', '2021-09-15 11:17:44', '2021-09-15 11:17:44');
INSERT INTO `t_pay_log` VALUES ('1439841989028831233', '20210920144029362', '2021-09-20 14:40:56', '0.01', '4200001166202109200897890633', 'SUCCESS', '1', '{\"transaction_id\":\"4200001166202109200897890633\",\"nonce_str\":\"LCxGTMXe7BbtqE7B\",\"trade_state\":\"SUCCESS\",\"bank_type\":\"OTHERS\",\"openid\":\"oHwsHuEpzOjI4QipIoXnrOLYx8dE\",\"sign\":\"AB1A46DA4281A942CB55097A5CE27D5B\",\"return_msg\":\"OK\",\"fee_type\":\"CNY\",\"mch_id\":\"1558950191\",\"cash_fee\":\"1\",\"out_trade_no\":\"20210920144029362\",\"cash_fee_type\":\"CNY\",\"appid\":\"wx74862e0dfcf69954\",\"total_fee\":\"1\",\"trade_state_desc\":\"支付成功\",\"trade_type\":\"NATIVE\",\"result_code\":\"SUCCESS\",\"attach\":\"\",\"time_end\":\"20210920144052\",\"is_subscribe\":\"N\",\"return_code\":\"SUCCESS\"}', '0', '2021-09-20 14:40:56', '2021-09-20 14:40:56');
