/*
Navicat MySQL Data Transfer

Source Server         : MyProject
Source Server Version : 50605
Source Host           : localhost:3306
Source Database       : gulixueyuan

Target Server Type    : MYSQL
Target Server Version : 50605
File Encoding         : 65001

Date: 2021-09-21 14:43:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for statistics_daily
-- ----------------------------
DROP TABLE IF EXISTS `statistics_daily`;
CREATE TABLE `statistics_daily` (
  `id` char(19) NOT NULL COMMENT '主键',
  `date_calculated` varchar(20) NOT NULL COMMENT '统计日期',
  `register_num` int(11) NOT NULL DEFAULT '0' COMMENT '注册人数',
  `login_num` int(11) NOT NULL DEFAULT '0' COMMENT '登录人数',
  `video_view_num` int(11) NOT NULL DEFAULT '0' COMMENT '每日播放视频数',
  `course_num` int(11) NOT NULL DEFAULT '0' COMMENT '每日新增课程数',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `statistics_day` (`date_calculated`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='网站统计日数据';

-- ----------------------------
-- Records of statistics_daily
-- ----------------------------
INSERT INTO `statistics_daily` VALUES ('1', '2021-09-10', '0', '2', '150', '1', '2021-09-15 15:05:43', '2021-09-15 15:05:45');
INSERT INTO `statistics_daily` VALUES ('1438035755808092161', '2021-09-11', '2', '96', '199', '3', '2021-09-15 15:03:36', '2021-09-15 15:03:36');
INSERT INTO `statistics_daily` VALUES ('1438040170254409730', '2021-09-08', '1', '91', '134', '4', '2021-09-15 15:21:09', '2021-09-15 15:21:09');
INSERT INTO `statistics_daily` VALUES ('1439051898534506498', '2021-09-15', '0', '61', '116', '1', '2021-09-18 10:21:24', '2021-09-18 10:21:24');
INSERT INTO `statistics_daily` VALUES ('2', '2021-09-09', '0', '3', '123', '5', '2021-09-15 15:06:26', '2021-09-15 15:06:28');
INSERT INTO `statistics_daily` VALUES ('4', '2021-09-07', '0', '3', '166', '10', '2021-09-15 15:07:57', '2021-09-15 15:07:59');
