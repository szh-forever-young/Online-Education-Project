/*
Navicat MySQL Data Transfer

Source Server         : MyProject
Source Server Version : 50605
Source Host           : localhost:3306
Source Database       : gulixueyuan

Target Server Type    : MYSQL
Target Server Version : 50605
File Encoding         : 65001

Date: 2021-09-21 14:43:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ucenter_member
-- ----------------------------
DROP TABLE IF EXISTS `ucenter_member`;
CREATE TABLE `ucenter_member` (
  `id` char(19) NOT NULL COMMENT '会员id',
  `openid` varchar(128) DEFAULT NULL COMMENT '微信openid',
  `mobile` varchar(11) DEFAULT '' COMMENT '手机号',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `sex` tinyint(2) unsigned DEFAULT NULL COMMENT '性别 1 女，2 男',
  `age` tinyint(3) unsigned DEFAULT NULL COMMENT '年龄',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像',
  `sign` varchar(100) DEFAULT NULL COMMENT '用户签名',
  `is_disabled` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否禁用 1（true）已禁用，  0（false）未禁用',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` datetime NOT NULL COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='会员表';

-- ----------------------------
-- Records of ucenter_member
-- ----------------------------
INSERT INTO `ucenter_member` VALUES ('1436582068313141250', null, '13101792642', 'e10adc3949ba59abbe56e057f20f883e', '张起灵', null, null, 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/03/7075de64b31045639b71144abce35e6dfile.png', null, '0', '0', '2021-09-11 14:47:10', '2021-09-11 14:47:10');
INSERT INTO `ucenter_member` VALUES ('1436594263268802561', null, '15036768671', '96e79218965eb72c92a549dd5a330112', '小哥', null, null, 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/03/7075de64b31045639b71144abce35e6dfile.png', null, '0', '0', '2021-09-11 15:35:38', '2021-09-11 15:35:38');
INSERT INTO `ucenter_member` VALUES ('1436950546622836738', 'o3_SC58IYbLJ1C705yTlf47dyIFw', '', null, '浩H', null, null, 'https://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83er8wmSj26rrwbfCZLIrq7Gv4LqN8UVdksU2AjtoGLZcoGicA6krxVJl8pD6CAicB3ic8srSlsvBR4Vng/132', null, '0', '0', '2021-09-12 15:11:22', '2021-09-12 15:11:22');
INSERT INTO `ucenter_member` VALUES ('1439851279982559233', null, '13693795651', '96e79218965eb72c92a549dd5a330112', '闷油瓶', null, null, 'https://gulixueyuanedu-1010.oss-cn-beijing.aliyuncs.com/2021/09/03/7075de64b31045639b71144abce35e6dfile.png', null, '0', '0', '2021-09-20 15:17:51', '2021-09-20 15:17:51');
